# Boxinator

Frontend part of the boxinator project.

This application designed for calculating the shipping cost for mystery boxes to specific locations around the world. It is a web application, using a RESTful API to communicate with a server.
For this case we designed, developed and distributed the solution. 
NB this if page does not accept any form of payment and is meerly for demonstation purposes. 

Consists of Login-page, verification-page, main-page for users, admin-page for admins, profile-page and guest-page.

There is a User manual for the application, that is upploaded to the repo, or found bellow.
Frontend of the application is hosted on heroku, and kan be found here:
https://boxinator-shipments.herokuapp.com

### API 

The api /backend is in c# Here we have use Entity framework code first approach to genereate msSQL relational database.
Api can be found here: https://gitlab.com/benjaminandersen/boxinator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.8.


## Development server

To setup the project run:

`npm install -g @angular/cli` To install angular

`npm i @angular-devkit/build-angular`     To install angular devkit

Then in the package.json file change if not already change:

    "start": "node server.js",

to:

    "start": "ng serve --ssl true",

Run `npm start` for a dev server. Navigate to `https://localhost:4200/`. The app will automatically reload if you change any of the source files.
This project should run on https, the reason for this is that the social auth services(google signin) requires https. Also the backend part of the project should also be set up for the app to propely work, and get seeding data. 
NB in environment.ts change this to connect to backend locally:

  accountsUrl: 'https://boxinatorshipments.azurewebsites.net/api/accounts/',

  countriesUrl: 'https://boxinatorshipments.azurewebsites.net/api/countries/',

  shipmentsUrl: 'https://boxinatorshipments.azurewebsites.net/api/shipments/',

  twoFactorUrl: 'https://boxinatorshipments.azurewebsites.net/api/twofactor/'


to this:

  accountsUrl: 'https://localhost:44387/api/accounts/',

  countriesUrl: 'https://localhost:44387/api/countries/',

  shipmentsUrl: 'https://localhost:44387/api/shipments/',
  
  twoFactorUrl: 'https://localhost:44387/api/twofactor/'

## User manual

User manual can be found here: https://gitlab.com/stiankvamme/boxinator-frontend/-/blob/master/user_manual_the_boxinator.pdf

## Login page

the loginpage allows users to sign in or register as a new user with their google account. This is the reason for https, if not google providers will not work. There is also an option to sing in as a guest user then only email will be stored for reciept purposes. when logged in, user will be redirected to verfication-page if they are registerd, or to registration page if not.

## Registration page

this allows users that have not yet registerd to fill in information about themself for shipping purposes. after this user is redirected to verificaiton page.

## Verification page

When signin in user is sendt and verification code on mail, that will be needed to typed in here. if the code is not sent, or invalid user can be sent a new code, this option will be displayed once the "send verification" button is pressed and if the code is invalid.
The Verification code is only valid for six minutes, after this it will be invalid.
After this user is redirected to either main-page or admin-page if user or admin

## Main page

Main page allows a user to add a new shipment, here they can specify weight option, and destination country aswell as the box color.
A user can also see all shipments that are not completed in a table, with the option to cancel a shipment, and all completed shipments in another table. From here a user can use the navbar to go to profile page or sign out.

## Admin page

From the main page an admin can view shipments for all users in a shipment table. An
admin can edit any shipment except their own shipments. To edit a shipment, the admin must click the
edit icon in the “Delete/Edit” column. A modal will then pop up and the admin can edit receiver name,
box tier, destination country, status and RGBA colour. A shipment can have five different
statuses:
• CREATED
• INTRANSIT
• RECEIVED
• COMPLETED
• CANCELLED
When the admin is finished editing the shipment they should click “Save”. An admin can also delete a
shipment by clicking the delete icon under the “Delete/Edit” column. An admin can delete their own
shipments. 

Admins can also add shipments, this works similarly to a users way of adding shippment, see usermanual for more detailed description

Under the “Add/Edit Country” section of the admin main page, an administrator can add a new country or edit
a country multiplier. The country multiplier is the multiplier which the weight of the
shipment will be multiplied with to calculate the cost of the shipment. To add a new country, the admin
must insert a country name and a country multiplier and click “Add country”. To edit a country, an
admin can select a country for the country dropdown menu and then insert a new country multiplier,
before clicking “Edit country”.

an admin can change the account type of any user under the “Change Account
Type” section. To choose an account to change the account type of the user can select an
account e-mail in the “Account E-mail” dropdown menu. The account ID and name of the selected
account will then de displayed. The admin can then choose between “Regular user” and “Administrator”
in the “Account Type” dropdown menu. After selecting an account type, the admin should click “Change
Account Type”. An admin cannot change their own account type.

An admin can
delete any account, something that should only be done in extreme situations as this action cannot be
undone. An admin cannot delete their own account. To delete an account, the admin must select the
account e-mail of the account they would like to delete under the “Account E-mail” dropdown menu.
The account ID and name will then be displayed. The admin should then click “Delete Account”. 

## Profile page

On the profile page, the user can view and update their account information. To update account
information, the user should click “Update account information” and the input fields will then be open
to edit. The user can then update their account information and click “Update” to save the
new information.

## Navbar

The nav bar lets user navigate between user page and main page(or admin main page if admin), guests can only se the logout button.
