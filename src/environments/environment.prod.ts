export const environment = {
  production: true,
  googleClient: '365483772148-f9564v4896cf1s6t819b5vppic29d2vn.apps.googleusercontent.com',
  accountsUrl: 'https://boxinatorshipments.azurewebsites.net/api/accounts/',
  countriesUrl: 'https://boxinatorshipments.azurewebsites.net/api/countries/',
  shipmentsUrl: 'https://boxinatorshipments.azurewebsites.net/api/shipments/',
  twoFactorUrl: 'https://boxinatorshipments.azurewebsites.net/api/twofactor/'
};
