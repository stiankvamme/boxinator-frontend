import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminComponent } from "./components/admin-page/admin/admin.component";
import { LoginComponent } from "./components/login-page/login.component";
import { MainComponent } from "./components/main-page/main/main.component";
import { ProfileComponent } from "./components/profile-page/profile/profile.component";
import { RegisterComponent } from "./components/register-page/register/register.component";
import { GuestComponent } from "./components/guest-page/guest/guest.component";
import { TwoStepVerficationPageComponent } from "./components/two-step-verfication-page/two-step-verfication-page.component";
import { AuthGuard } from "./services/auth.guard";

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'main',
        component: MainComponent,
        canActivate: [AuthGuard]

    },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'guest',
        component: GuestComponent
    },
    {
        path: 'verification',
        component: TwoStepVerficationPageComponent
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'login'
    }

]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{};