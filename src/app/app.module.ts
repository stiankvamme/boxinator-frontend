import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login-page/login.component';
import { RegisterComponent } from './components/register-page/register/register.component';
import { AdminComponent } from './components/admin-page/admin/admin.component';
import { ProfileComponent } from './components/profile-page/profile/profile.component';
import { AppRoutingModule } from './app-routing.module';
import { ValidateEqualModule } from 'ng-validate-equal';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContainershipmentsComponent, CountryIdToNamePipe } from './components/container-shipments/containershipments/containershipments.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { MainComponent } from './components/main-page/main/main.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { AddShipmentComponent } from './components/add-shipment/add-shipment.component';
import { GuestComponent } from './components/guest-page/guest/guest.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditShipmentComponent } from './components/edit-shipment/edit-shipment.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { TwoStepVerficationPageComponent } from './components/two-step-verfication-page/two-step-verfication-page.component';
import { CompletedShipmentsComponent } from './components/completed-shipments/completed-shipments.component';
import { ShipmentHistoryComponent } from './components/shipment-history/shipment-history.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MainComponent,
    AdminComponent,
    ProfileComponent,
    NavbarComponent,
    ContainershipmentsComponent,
    AddShipmentComponent,
    GuestComponent,
    EditShipmentComponent,
    CountryIdToNamePipe,
    TwoStepVerficationPageComponent,
    CompletedShipmentsComponent,
    ShipmentHistoryComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ValidateEqualModule,
    ColorPickerModule,
    SocialLoginModule,
    BrowserAnimationsModule,
    MatDialogModule,
    CommonModule
  ],
  providers: [ {
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '365483772148-f9564v4896cf1s6t819b5vppic29d2vn.apps.googleusercontent.com'
          )
        },
      ],
    } as SocialAuthServiceConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
