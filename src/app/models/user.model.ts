export interface User {
    id?: number,
    firstName: string,
    lastName: string, 
    email: string,
    dateOfBirth: string,
    country: string,
    postalCode: string,
    contactNumber: string,
    accountType?: number
}