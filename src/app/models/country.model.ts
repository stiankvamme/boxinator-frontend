export interface Country {
    id?: number,
    countryName: string,
    countryMultiplier: number
}