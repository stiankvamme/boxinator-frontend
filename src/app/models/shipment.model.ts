export interface Shipment {
    id?: number,
    receiverName: string,
    status: number, 
    destinationCountry: number,
    weight_kg: number,
    boxColour: string,
    price:  number,
    accountId: number,
    date?: Date
}
