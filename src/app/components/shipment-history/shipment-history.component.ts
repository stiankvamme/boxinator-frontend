import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MainService } from 'src/app/services/main.service';
import { DialogData } from '../edit-shipment/edit-shipment.component';

@Component({
  selector: 'app-shipment-history',
  templateUrl: './shipment-history.component.html',
  styleUrls: ['./shipment-history.component.css']
})
export class ShipmentHistoryComponent implements OnInit {

  constructor(
    private readonly mainService: MainService,
    public dialogRef: MatDialogRef<ShipmentHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  // for testing
  public statusHistory = [{date: "02/04/21", status: "CREATED"}, {date: "04/04/21", status: "INTRANSIT"}]

  //public statusHistory: StatusHistory[] = [];

  ngOnInit(): void {
    // get status history
    // this.mainService.getShipmentHistory( () => {
    //   this.statusHistory = this.statusHistoryFromService;
    // });
  }

  // get shipment statuses from mainService
  // get statusHistoryFromService() : StatusHistory[] | undefined {
  //   return this.mainService.shipmentHistory(this.data.shipment.id);
  // }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
