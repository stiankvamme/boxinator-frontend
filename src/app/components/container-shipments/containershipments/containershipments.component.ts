import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { Shipment } from 'src/app/models/shipment.model';
import { MainService } from 'src/app/services/main.service';
import { MatDialog } from '@angular/material/dialog';
import { EditShipmentComponent } from '../../edit-shipment/edit-shipment.component';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';
import { Country } from 'src/app/models/country.model';
import { CountryService } from 'src/app/services/countries.service';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { RgbaValidationService } from 'src/app/services/rgbaValidation.service';
import { ShipmentHistoryComponent } from '../../shipment-history/shipment-history.component';

@Component({
  selector: 'app-containershipments',
  templateUrl: './containershipments.component.html',
  styleUrls: ['./containershipments.component.css'],
})
export class ContainershipmentsComponent implements OnInit {

  constructor(
    private readonly mainService: MainService,
    private readonly countryService: CountryService,
    private readonly sessionService: SessionService,
    private readonly rgbaValidationService: RgbaValidationService,
    public dialog: MatDialog
  ) { }

  public user: User | undefined;
  private googleUser: GoogleUser | undefined;
  public countries: Country[] = [];
  private statusStrings = [
    "CREATED", 
    "RECIEVED", 
    "INTRANSIT", 
    "COMPLETED",
    "CANCELLED"
  ];
  private boxTierStrings = [
    "Basic - 1 KG",
    "Humble - 2 KG",
    "Deluxe - 5 KG",
    "Premium - 8 KG"
  ]

  @Input() public completed: boolean = false;

  public isAdmin = false;
  public activeShipments = true;
  public completedShipments = false;
  public cancelledShipments = false;

  // get active shipments from mainService 
  get activeShipmentsFromService () : Shipment[] | undefined {
    return this.mainService.activeShipments();
  }

  // get completed shipments from mainService
  get completedShipmentsFromService () : Shipment[] | undefined {
    return this.mainService.completedShipments();
  }

  // get cancelled shipments from mainService
  get cancelledShipmentsFromService () : Shipment[] | undefined {
    return this.mainService.cancelledShipments();
  }

  // get specific user's non-completed shipments from mainService
  get userShipmentsFromService () : Shipment[] | undefined {
    return this.mainService.nonCompletedShipments();
  }

  // get all countries
  get countriesFromService () : Country[] | undefined {
    return this.countryService.countries();
  }

  // check if admin or user and set shipments accordingly
  get shipments(): Shipment[] | undefined {
    if (!this.isAdmin) {
      return this.userShipmentsFromService;
    } else {
      // check if active, completed or cancelled shipments should be displayed
      if (this.completedShipments) {
        return this.completedShipmentsFromService;
      } else if (this.cancelledShipments) {
        return this.cancelledShipmentsFromService;
      } else {
        return this.activeShipmentsFromService;
      }
    }
  }

  ngOnInit(): void {
    // get user from session
    this.user = this.sessionService.User;

    // get google user from session
    this.googleUser = this.sessionService.googleUser;

    if (this.user?.accountType === 2) {
      this.isAdmin = true;
    }
    
    // get countries
    this.countryService.getCountries( () => {
      this.countries = this.countriesFromService ?? [];
    });

    // if user is admin, shipments should be all shipments
    if (this.isAdmin && this.googleUser?.idToken && this.user?.id) {
      this.mainService.getActiveShipments(this.user?.id, this.googleUser?.idToken, () => {
        console.log("Fetched all active shipments");
      });
    } else if (this.user?.id && !this.isAdmin && this.googleUser?.idToken) {
      this.mainService.getNonCompletedShipments(this.user.id, this.user.id, this.googleUser?.idToken, () => {
        console.log("Fetched non-completed shipments");
      });
    }
  }

  // when admin wants to see all active shipments
  onActiveClick() {
    this.activeShipments = true;
    this.completedShipments = false;
    this.cancelledShipments = false;

    if (this.isAdmin && this.googleUser?.idToken && this.user?.id) {
      this.mainService.getActiveShipments(this.user?.id, this.googleUser?.idToken, () => {
        console.log("Fetched all active shipments");
      });
    }
  }

  // when admin want to see all completed shipments
  onCompletedClick() {
    this.activeShipments = false;
    this.completedShipments = true;
    this.cancelledShipments = false;

    if (this.isAdmin && this.googleUser?.idToken && this.user?.id) {
      this.mainService.getCompletedShipments(this.user?.id, this.googleUser?.idToken, () => {
        console.log("Fetched all completed shipments");
      });
    }
  }

  // when admin wants to see all cancelled shipments
  onCancelledClick() {
    this.activeShipments = false;
    this.completedShipments = false;
    this.cancelledShipments = true;

    if (this.isAdmin && this.googleUser?.idToken && this.user?.id) {
      this.mainService.getCancelledShipments(this.user?.id, this.googleUser?.idToken, () => {
        console.log("Fetched all cancelled shipments");
      });
    }
  }

  // open dialog to edit a shipment - for admins
  openDialogEdit(shipment : Shipment) {
    const dialogRef = this.dialog.open(EditShipmentComponent, {
      // Clone shipment, and send copy to dialog
      data: { shipment: JSON.parse(JSON.stringify(shipment)) }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        const id = result.id;
        const receiverName = result.receiverName;
        const weight_kg = result.weight_kg;
        const destinationCountry = result.destinationCountry;
        const status = Number(result.status);
        const boxColour = this.rgbaValidationService.validateRGBA(result.boxColour);
        const accountId = result.accountId;
        //calculate shipping cost
        let countryMultiplier = 0;
        if(this.countries) {
          countryMultiplier = this.countries.
            find(c => c.id === Number(result.destinationCountry))?.countryMultiplier ?? 1;
        }
        const price = Number(result.weight_kg) * Number(countryMultiplier) + 200;
        
        // created updated shipment
        const updatedShipment: Shipment = {
          id,
          receiverName, 
          status, 
          destinationCountry, 
          weight_kg, 
          boxColour, 
          price,
          accountId
        };

        // find userId of logged in user
        const userId = this.user?.id;
        const googleId = this.googleUser?.idToken;

        if (userId && googleId) {
          // update shipment in api
          this.mainService.updateShipmentAdmin(updatedShipment, userId, googleId, () => {
            alert("Shipment was updated");
            // get all shipments again
            this.mainService.getActiveShipments(userId, googleId, () => {
              console.log("Fetched active shipments")
            });
            this.mainService.getCompletedShipments(userId, googleId, () => {
              console.log("Fetched completed shipments")
            });
            this.mainService.getCancelledShipments(userId, googleId, () => {
              console.log("Fetched cancelled shipments")
            });
          });
        }
      }
    });
  }

  // show status as string
  translateShipmentStatus(status : number) {
    return this.statusStrings[status]
  }

  // show box weight as string
  translateBoxWeight(weight : number) {
    if (weight === 1) {
      return this.boxTierStrings[0]
    } else if (weight === 2) {
      return this.boxTierStrings[1]
    } else if (weight === 5) {
      return this.boxTierStrings[2]
    } else if (weight === 8) {
      return this.boxTierStrings[3]
    }
    return weight;
  }

  // cancel shipment - for regular users
  public onCancelClick(shipment : Shipment){
    // check if shipment status is completed
    const warning_text = "Are you sure you want to cancel shipment? This action cannot be undone";

    // userId of the logged in user
    const userId = this.user?.id;

    // get logged in google user's idToken
    const googleUserId = this.sessionService.googleUser?.idToken;

    if (confirm(warning_text) && userId && googleUserId) {
      this.mainService.updateShipment(shipment, userId, googleUserId, () => {
        alert("Shipment was cancelled");
        // update shipments
        this.mainService.getNonCompletedShipments(userId, userId, googleUserId, () => {
          console.log("Fetched shipments")
        });
      });
    } 
  }

  // delete shipment - for admins
  public onDeleteShipmentClick(shipmentId : number | undefined) {
    const warning_text = "Are you sure you want to delete shipment? This action cannot be undone"

    // get logged in google user's idToken
    const googleId = this.googleUser?.idToken;

    // get logged in user's id
    const userId = this.user?.id

    if (confirm(warning_text) && shipmentId && googleId && userId) {
      this.mainService.deleteShipment(shipmentId , userId, googleId, () => {
        alert("Shipment was deleted");
        // update shipments
        this.mainService.getActiveShipments(userId, googleId, () => {
          console.log("Fetched active shipments")
        });
        this.mainService.getCompletedShipments(userId, googleId, () => {
          console.log("Fetched completed shipments")
        });
        this.mainService.getCancelledShipments(userId, googleId, () => {
          console.log("Fetched cancelled shipments")
        });
      });
    } 
  }

  // open dialog to see shipment history
  openDialogHistory(shipment: Shipment) {
    const dialogRef = this.dialog.open(ShipmentHistoryComponent, {
      // Clone shipment, and send copy to dialog
      data: { shipment: JSON.parse(JSON.stringify(shipment)) }
    });
  }
}

// to get country name in country selects instead of country id
@Pipe({name: 'countryIdToName'})
export class CountryIdToNamePipe implements PipeTransform {
  transform(countryId: number, countries: Country[]): string {
    return countries.find(c => c.id === countryId)?.countryName ?? " "
  }
}
