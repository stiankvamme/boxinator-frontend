import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainershipmentsComponent } from './containershipments.component';

describe('ContainershipmentsComponent', () => {
  let component: ContainershipmentsComponent;
  let fixture: ComponentFixture<ContainershipmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainershipmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainershipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
