import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { User } from 'src/app/models/user.model';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  constructor(
    private readonly router: Router,
    private readonly registerService: RegisterService,
    private readonly sessionService: SessionService
  ) {}

    public googleUser: GoogleUser | undefined;
    private loggedInUser: User | undefined;

  ngOnInit(): void {
    // get google user from localstorage
    this.googleUser = this.sessionService.googleUser;
    // get logged in user from local storage
    this.loggedInUser = this.sessionService.User;

    // check logged in user's account type and redirect
    if (this.loggedInUser && this.loggedInUser.accountType === 1) {
      this.router.navigate(['main']);
    } else if (this.loggedInUser && this.loggedInUser.accountType === 2) {
      this.router.navigate(['admin']);
    }
  }

  // get user from register service
  get userFromServer(): User | undefined{
    return this.registerService.User();
  }

  // submit register form
  onSubmit(registerForm: NgForm): void {
    const { firstName } = registerForm.value;
    const { lastName } = registerForm.value;
    const { email } = registerForm.value;
    const { dateOfBirth } = registerForm.value;
    const { country } = registerForm.value;
    const { postalCode } = registerForm.value;
    const { contactNumber } = registerForm.value;

    // get google id from logged in google user
    const googleId = this.googleUser?.idToken;
  
    // check that all requirements are not empty
    if (firstName && lastName && email && googleId) {
       
      // create a new user object with the inputted data
      const registeredUser: User = {firstName, lastName, email, dateOfBirth, country, postalCode, contactNumber};
      registeredUser.accountType = 1; //accountType 1 = regular user
      
      // call registerUser method with the created user and navigate to login page so that user can login
      this.registerService.registerUser(registeredUser, googleId, () => {
        alert("Registration successful")
        this.sessionService.setUser(this.userFromServer);
        this.router.navigate(['verification']);
      });
    }
  }
}
