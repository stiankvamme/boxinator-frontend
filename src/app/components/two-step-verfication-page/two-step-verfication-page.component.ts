import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';
import { TwoStepVerificationService } from 'src/app/services/twoStepVerification.service';

@Component({
  selector: 'app-two-step-verfication-page',
  templateUrl: './two-step-verfication-page.component.html',
  styleUrls: ['./two-step-verfication-page.component.css'],
})
export class TwoStepVerficationPageComponent implements OnInit {
  constructor(
    private readonly twoStepVerification: TwoStepVerificationService,
    private readonly sessionService: SessionService,
    private router: Router
  ) {}

  public codeAgain: Boolean = false;
  private user: User | undefined;
  private googleUser: GoogleUser | undefined;

  //get twoFA true/false from localstorage
  get twoFAFromServer(): Boolean | undefined {
    return this.twoStepVerification.TwoFA();
  }

  ngOnInit(): void {
    // get user and google user from local storage
    this.googleUser = this.sessionService.googleUser;
    this.user = this.sessionService.User;
  }

  //submit verification code
  onSubmit(verificationForm: NgForm): void {
    const { verificationCode } = verificationForm.value;
    //send validation code to server
    if (this.googleUser && this.user?.id) {
      this.twoStepVerification.validateVerificationCode(
        this.googleUser,
        verificationCode,
        this.user.id,
        () => {
          this.sessionService.setTwoFA(this.twoFAFromServer);
          // redirect user if user has a twoFA object on localstorage that is true, if true then validation code was ok
          if (this.twoFAFromServer) { 
            if (this.user?.accountType === 2) {
              setTimeout(() => this.router.navigate(['admin']), 500);
            } else if (this.user?.accountType === 1) {
              setTimeout(() => this.router.navigate(['main']), 500);
            }
          } else {
            this.codeAgain = true;
          }
        }
      );
    }
  }

  //send new code
  public onClickNewCode() {
    if (this.googleUser && this.user?.id) {
      this.twoStepVerification.getNewVerificationCode(
        this.googleUser,
        this.user.id,
        () => {
          alert('New code sent');
        }
      );
    }
  }
}
