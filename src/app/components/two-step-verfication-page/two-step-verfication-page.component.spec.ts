import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoStepVerficationPageComponent } from './two-step-verfication-page.component';

describe('TwoStepVerficationPageComponent', () => {
  let component: TwoStepVerficationPageComponent;
  let fixture: ComponentFixture<TwoStepVerficationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TwoStepVerficationPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoStepVerficationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
