import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private readonly router: Router, 
    private readonly sessionService: SessionService, 
    private readonly authenticationService: AuthenticationService, 
    private authService: SocialAuthService
  ) { }

  public loggenInUser: User | undefined;

  ngOnInit(): void {
    // get logged in user from local storage
    this.loggenInUser = this.sessionService.User;
  }

  // navigate to main when clicking "Shipment Status" - should only be visible to regular users
  onMainClick() {
    this.router.navigate(['main']);
  }

  // navigate to admin page when clicking "Admin page" - onle visible to admin users
  onAdminClick() {
    this.router.navigate(['admin']);
  }

  // navigate to profile when clicking "User Account" - should only be visible to logged in users (not guests)
  onUserClick() {
    this.router.navigate(['profile']);
  }

  // when clicking on "The Boxinator" if admin redirect to admin page, if user redirect to main page
  onLogoClick() {
    if (this.loggenInUser?.accountType === 1) {
      this.router.navigate(['main']);
    } else if (this.loggenInUser?.accountType === 2) {
      this.router.navigate(['admin']);
    } else if (this.loggenInUser?.accountType === 0) {
      this.router.navigate(['login']);
    }
  }

  // should logout a user and navigate back to login page
  async onLogoutClick() {
    this.sessionService.logOut();
    this.authenticationService.signOutExternal();
    this.authService.signOut(true)
    this.router.navigate(['login']);
  }
}
