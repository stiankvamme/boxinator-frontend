import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Country } from 'src/app/models/country.model';
import { Shipment } from 'src/app/models/shipment.model';
import { CountryService } from 'src/app/services/countries.service';

export interface DialogData {
  shipment: Shipment
}

@Component({
  selector: 'app-edit-shipment',
  templateUrl: './edit-shipment.component.html',
  styleUrls: ['./edit-shipment.component.css']
})
export class EditShipmentComponent implements OnInit{

  constructor(
    public dialogRef: MatDialogRef<EditShipmentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public readonly countryService: CountryService
  ) {}

  private statusStrings = [
    "CREATED", 
    "RECIEVED", 
    "INTRANSIT", 
    "COMPLETED",
    "CANCELLED"
  ];
  public statusList = [0, 1, 2, 3, 4];
  public countries!: Country[] | undefined;
  public cost: number = this.data.shipment.price;

  // get all countries to show in country select
  get countriesFromService () : Country[] | undefined {
    return this.countryService.countries();
  }

  ngOnInit(): void {
    // get countries
    this.countryService.getCountries( () => {
      this.countries = this.countriesFromService;
    });
  }

  // show status as string
  translateShipmentStatus(status : number) {
    return this.statusStrings[status]
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }

  // update cost when weight is changed
  updateCostWeightChange(newWeight: any): void {
    if(this.countries) {
      const countryMultiplier = this.countries.
        find(c => c.id === Number(this.data.shipment.destinationCountry))?.countryMultiplier ?? 1;
      this.cost = Number(newWeight) * countryMultiplier + 200;
    }
  }

  // update cost when country is changed
  updateCostCountryChange(newCountry: any): void {
    if(this.countries) {
      const countryMultiplier = this.countries.find(c => c.id === Number(newCountry))?.countryMultiplier ?? 1;
      this.cost = countryMultiplier * this.data.shipment.weight_kg + 200;
    }
  }
}
