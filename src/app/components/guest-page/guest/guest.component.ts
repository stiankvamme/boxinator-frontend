import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Country } from 'src/app/models/country.model';
import { MainService } from 'src/app/services/main.service';
import { RegisterService } from 'src/app/services/register.service';
import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { Shipment } from 'src/app/models/shipment.model';
import { CountryService } from 'src/app/services/countries.service';
import { RgbaValidationService } from 'src/app/services/rgbaValidation.service';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.css'],
})
export class GuestComponent implements OnInit {
  constructor(
    public vcRef: ViewContainerRef,
    private readonly router: Router,
    private readonly mainService: MainService,
    private readonly registerService: RegisterService,
    private readonly sessionService: SessionService,
    private readonly countryService: CountryService,
    private readonly rgbaValidationService: RgbaValidationService
  ) {}

  public countries: Country[] | undefined;
  public _guestUser: User | undefined;
  public selectedCountry!: Country;
  private loggedInUser: User | undefined;
  public selectedColor = 'rgba(99,186,205,1)';
  public cost = 0;
  public selectedWeight = 0;

  ngOnInit(): void {
    // get countries from country service
    this.countryService.getCountries(() => {
      this.countries = this.countriesFromService;
    });

    // get logged in user
    this.loggedInUser = this.sessionService.User;

    // check logged in user's account type and redirect
    if (this.loggedInUser && this.loggedInUser.accountType === 1) {
      this.router.navigate(['main']);
    } else if (this.loggedInUser && this.loggedInUser.accountType === 2) {
      this.router.navigate(['admin']);
    }
  }

  // get all countries
  get countriesFromService(): Country[] | undefined {
    return this.countryService.countries();
  }

  // add new shipment
  public async onSubmit(shipmentForm: NgForm): Promise<void> {
    const status = 0; //created
    const email = shipmentForm.value.email;
    const receiverName = shipmentForm.value.fullname;
    const destinationCountry = Number(this.selectedCountry.id); 
    const weight_kg = shipmentForm.value.weight;
    const boxColour = this.rgbaValidationService.validateRGBA(this.selectedColor);
    const price = this.cost;
    const accountId = 0;
    const newShipment: Shipment = {
      receiverName,
      status,
      destinationCountry,
      weight_kg,
      boxColour,
      price,
      accountId,
    };
    if (email === '') {
      alert('Error: please type email');
      return;
    }
    if (boxColour === '') {
      alert('Error: please type valid rgba(color) value');
      return;
    }

    // check that requirements are not empty
    if (email !== '') {
      // call registerGuestUser method with the created user
      this.registerService.registerGuestUser(email, () => {
        this.sessionService.setUser(this.userFromServer);
        this._guestUser = this.sessionService.User;
        newShipment.accountId = this._guestUser?.id || 0;
        if (!this._guestUser) {
          alert('Error: Something went wrong, please try again');
          return;
        }
        // add new shipment
        this.mainService.addShipment(newShipment, () => {
          alert("Shipment was added")
          shipmentForm.reset();
        });
      });
    }
  }
  //navigate to login
  public onRegisterClick() {
    this.router.navigate(['login']);
  }

  // calculate the shipping cost
  public calculateCost(weight: number, multiplier: number): number {
    if (weight > 0 && multiplier > 0) {
      this.cost = weight * multiplier + 200;
    }
    return this.cost
  }

   // when country is changed in country select
   onCountryChange(event : Country) {
    this.selectedCountry = event;
    if (this.selectedWeight) {
      this.cost = this.calculateCost(this.selectedWeight, this.selectedCountry.countryMultiplier)
    }
  }

  // when weight is changed in weight select
  onWeightChange(event : any) {
    this.selectedWeight = event.value;
    if (this.selectedCountry) {
      this.cost = this.calculateCost(this.selectedWeight, this.selectedCountry.countryMultiplier)
    }
  }

  // when colour is changed in the colour input
  public onChangeColor(color: string): void {
    this.selectedColor=color;
  }

  // get user from register service
  get userFromServer(): User | undefined {
    return this.registerService.User();
  }
}
