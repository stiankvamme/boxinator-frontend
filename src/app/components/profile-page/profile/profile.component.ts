import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    private readonly userService: UserService, 
    private readonly sessionService: SessionService, 
    private readonly router: Router,
  ) { }

  // updateInfo is false until user clicks "update account info"
  updateInfo = false;

  public user: User | undefined;
  private twoFA: Boolean | undefined

  // get user from userService
  get userFromService () : User | undefined {
    return this.userService.user();
  }

  ngOnInit(): void {
    this.user = this.sessionService.User;
    this.twoFA = this.sessionService.TwoFA;
    // navigate to login if no user logged in, if user is guest
    if(!this.user || this.user.accountType === 0){
      this.router.navigate(['login']);
    } 
  }

  // get updated user info and send updated user object to userService.updateUser
  onSubmitUpdate(updateForm: NgForm): void {
    const { firstName } = updateForm.value;
    const { lastName } = updateForm.value;
    const { email } = updateForm.value;
    const { dateOfBirth } = updateForm.value;
    const { country } = updateForm.value;
    const { postalCode } = updateForm.value;
    const { contactNumber } = updateForm.value;
    const id = this.user?.id;

    // get logged in google user's idToken
    const googleUserId = this.sessionService.googleUser?.idToken;
  
    // check that requirements are not empty
    if (firstName && lastName && email && id) {
      // create an updated user object with the inputted data
      const updatedUser: User = {id, firstName, lastName, email, dateOfBirth, country, postalCode, contactNumber};
      console.log(updatedUser);

      // call updateUser method with the updated user and set updateInfo to false
      if (googleUserId) {
        this.userService.updateUser(updatedUser, googleUserId, () => {
          if (updatedUser.id) {
            this.userService.getUser(updatedUser.id, updatedUser.id, googleUserId, () => {
              this.user = this.userFromService;
              this.sessionService.setUser(this.user);
            });
          }
          this.updateInfo = false;
        });
      }
    }
  }
}
