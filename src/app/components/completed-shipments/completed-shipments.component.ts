import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/models/country.model';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { Shipment } from 'src/app/models/shipment.model';
import { User } from 'src/app/models/user.model';
import { CountryService } from 'src/app/services/countries.service';
import { MainService } from 'src/app/services/main.service';
import { SessionService } from 'src/app/services/session.service';
import { ShipmentHistoryComponent } from '../shipment-history/shipment-history.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-completed-shipments',
  templateUrl: './completed-shipments.component.html',
  styleUrls: ['./completed-shipments.component.css']
})
export class CompletedShipmentsComponent implements OnInit {

  constructor(
    private readonly mainService: MainService,
    private readonly countryService: CountryService,
    private readonly sessionService: SessionService,
    public dialog: MatDialog
  ) { }

  public completedShipments: Shipment[] | undefined;
  private user: User | undefined;
  private googleUser: GoogleUser | undefined;
  public countries: Country[] = [];
  private statusStrings = [
    "CREATED", 
    "RECIEVED", 
    "INTRANSIT", 
    "COMPLETED",
    "CANCELLED"
  ];
  private boxTierStrings = [
    "Basic - 1 KG",
    "Humble - 2 KG",
    "Deluxe - 5 KG",
    "Premium - 8 KG"
  ]

  // get completed shipments from mainService
  get completedShipmentsForUserFromService () : Shipment[] | undefined {
    return this.mainService.completedShipmentsForUser();
  }

  // get all countries
  get countriesFromService () : Country[] | undefined {
    return this.countryService.countries();
  }

  ngOnInit(): void {
    // get user from session
    this.user = this.sessionService.User;

    // get google user from session
    this.googleUser = this.sessionService.googleUser;

    // get countries
    this.countryService.getCountries( () => {
      this.countries = this.countriesFromService ?? [];
    });

    // get completed shipments for user
    if (this.googleUser?.idToken && this.user?.id) {
      this.mainService.getCompletedShipmentsForUser(this.user.id, this.user.id, this.googleUser.idToken, () => {
        this.completedShipments = this.completedShipmentsForUserFromService;
      });
    }
  }

  // show status as string
  translateShipmentStatus(status : number) {
    return this.statusStrings[status]
  }

  // show box weight as string
  translateBoxWeight(weight : number) {
    if (weight === 1) {
      return this.boxTierStrings[0]
    } else if (weight === 2) {
      return this.boxTierStrings[1]
    } else if (weight === 5) {
      return this.boxTierStrings[2]
    } else if (weight === 8) {
      return this.boxTierStrings[3]
    }
    return weight;
  }

  // open dialog to see shipment history
  openDialogHistory(shipment: Shipment) {
    const dialogRef = this.dialog.open(ShipmentHistoryComponent, {
      // Clone shipment, and send copy to dialog
      data: { shipment: JSON.parse(JSON.stringify(shipment)) }
    });
  }
}
