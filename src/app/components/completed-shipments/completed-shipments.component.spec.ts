import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedShipmentsComponent } from './completed-shipments.component';

describe('CompletedShipmentsComponent', () => {
  let component: CompletedShipmentsComponent;
  let fixture: ComponentFixture<CompletedShipmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompletedShipmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedShipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
