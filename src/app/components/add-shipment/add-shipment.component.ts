import { Component, Input, OnInit, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Country } from 'src/app/models/country.model';
import { MainService } from 'src/app/services/main.service';
import { CountryService } from 'src/app/services/countries.service';
import { Shipment } from 'src/app/models/shipment.model';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { RgbaValidationService } from 'src/app/services/rgbaValidation.service';

@Component({
  selector: 'app-add-shipment',
  templateUrl: './add-shipment.component.html',
  styleUrls: ['./add-shipment.component.css']
})
export class AddShipmentComponent implements OnInit {

  constructor(
    public vcRef: ViewContainerRef,
    private readonly mainService : MainService,
    private readonly countryService: CountryService,
    private readonly sessionService: SessionService,
    private readonly userService: UserService,
    private readonly rgbaValidationService: RgbaValidationService
  ) {}

  public user: User | undefined;
  public users: User[] | undefined;
  public selectedUser: User | undefined;
  private googleUser: GoogleUser | undefined;
  private loggedInUser: User | undefined;
  public countries: Country[] | undefined;
  public selectedCountry!: Country;
  public selectedWeight: number = 0;
  public selectedColor: string = "rgba(99,186,205,1)"

  public cost: number = 0;
  
  // isAdmin is true/false depending on logged in user
  public isAdmin = false;

  @Input('countriesInp')
  set countriesInp(countriesInp: Country[] | undefined){
    this.countries=countriesInp || undefined;
  }

  ngOnInit(): void {
    // get user from session
    this.user = this.sessionService.User;

    if (this.user?.accountType === 2) {
      this.isAdmin = true;
    }

    // get countries
    this.countryService.getCountries( () => {
      this.countries = this.countriesFromService;
    });

    // get googleUser from local storage
    this.googleUser = this.sessionService.googleUser;

    // get logged in user from local storage
    this.loggedInUser = this.sessionService.User;

    // get users if logged in user is admin
    if(this.googleUser?.idToken && this.loggedInUser?.id && this.loggedInUser.accountType === 2) {
      this.userService.getUsers(this.loggedInUser.id, this.googleUser.idToken, () => {
        this.users = this.usersFromService;
      });
    }
  }

  // get all countries from country service
  get countriesFromService () : Country[] | undefined {
    return this.countryService.countries();
  }

  // get all users from user service
  get usersFromService () : User[] | undefined {
    return this.userService.users();
  }

  // calculate the shipping cost
  public calculateCost(weight: number, multiplier: number): number {
    if (weight > 0 && multiplier > 0) {
      this.cost = weight * multiplier + 200;
    }
    return this.cost
  }

  // when user is changed in user select
  onUserChange(event : User) {
    this.selectedUser = event;
  }

  // when country is changed in country select
  onCountryChange(event : Country) {
    this.selectedCountry = event;
    if (this.selectedWeight) {
      this.cost = this.calculateCost(this.selectedWeight, this.selectedCountry.countryMultiplier)
    }
  }

  // when weight is changed in weight select
  onWeightChange(event : any) {
    this.selectedWeight = event.value;
    if (this.selectedCountry) {
      this.cost = this.calculateCost(this.selectedWeight, this.selectedCountry.countryMultiplier)
    }
  }

  // when color is changed in color input
  onColorChange(color: string) {
    this.selectedColor = color;
  }

  // add new shipment
  public onSubmit(shipmentForm: NgForm): void {
    const { receiverName } = shipmentForm.value;
    const status = 0; //status 0 = created
    const destinationCountry = Number(this.selectedCountry.id); 
    const weight_kg = Number(this.selectedWeight);
    const boxColour = this.rgbaValidationService.validateRGBA(this.selectedColor);
    const price = this.cost;
    let accountId = 0;

    // if admin is logged in, get account id from input form
    if (this.isAdmin) {
      accountId = this.selectedUser?.id ?? 0;
    } else {
      // logged in user's id
      accountId = this.user?.id ?? 0; 
    }

    // create new shipment
    const newShipment: Shipment = {
      receiverName, 
      status, 
      destinationCountry, 
      weight_kg, 
      boxColour, 
      price,
      accountId
    };
    
    //check if color is blank
    if (boxColour != "") {
      this.mainService.addShipment(newShipment, () => {
        alert("Shipment was added");
        const token = this.googleUser?.idToken;
        const userId = this.loggedInUser?.id;
          if(token && userId && this.user?.id) {
            this.mainService.getActiveShipments(userId, token, () => console.log("Updated active shipments"));
            this.mainService.getCompletedShipments(userId, token, () => console.log("Updated completed shipments"));
            this.mainService.getCancelledShipments(userId, token, () => console.log("Updated cancelled shipments"));
            this.mainService.getNonCompletedShipments(userId, userId, token, () => console.log("Updated non-completed shipments"));
          }
        shipmentForm.reset();
      });
    }
  }
}