import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {

  constructor(
    private readonly sessionService : SessionService,
    private readonly router: Router
  ) {}

  private loggedInUser: User | undefined;
  private twoFA: Boolean | undefined
  
  ngOnInit(): void {
    // get logged in user from local storage
    this.loggedInUser = this.sessionService.User;
    this.twoFA = this.sessionService.TwoFA;

    // redirect admin/guest if they are not regular users
    if (this.loggedInUser?.accountType === 2 && this.twoFA) {
      this.router.navigate(['admin']);
    } else if(this.loggedInUser?.accountType === 0) {
      this.router.navigate(['guest']);
    }
  }
}
