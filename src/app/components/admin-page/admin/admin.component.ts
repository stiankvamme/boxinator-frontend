import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Country } from 'src/app/models/country.model';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { User } from 'src/app/models/user.model';
import { CountryService } from 'src/app/services/countries.service';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent implements OnInit {
  constructor(
    private readonly countryService: CountryService,
    private readonly userService: UserService,
    private readonly sessionService: SessionService,
    private router: Router
  ) {}

  public users: User[] | undefined;
  public userName: string | undefined;
  public userNameDelete: string | undefined;
  public selectedUser: User | undefined;
  public selectedUserDelete: User | undefined;
  private googleUser: GoogleUser | undefined;
  private loggedInUser: User | undefined;
  private twoFA: Boolean | undefined;
  public countries: Country[] | undefined;
  public selectedCountry!: Country;
  public selectedUserType: number = 0;

  // get countries from countryService
  get countriesFromService(): Country[] | undefined {
    return this.countryService.countries();
  }

  // get users from userService
  get usersFromService(): User[] | undefined {
    return this.userService.users();
  }

  ngOnInit(): void {
    // get countries
    this.countryService.getCountries(() => {
      this.countries = this.countriesFromService;
    });

    //get if twoFA
    this.twoFA = this.sessionService.TwoFA;

    // get google user from local storage
    this.googleUser = this.sessionService.googleUser;
    // get logged in user from local storage
    this.loggedInUser = this.sessionService.User;

    // get users
    if (this.googleUser?.idToken && this.loggedInUser?.id) {
      this.userService.getUsers(this.loggedInUser?.id, this.googleUser?.idToken, () => {
        this.users = this.usersFromService;
      });
    }

    //redirect to main page if user is regular user
    if (this.loggedInUser?.accountType === 1 && this.twoFA) {
      this.router.navigate(['main']);
    }

    //redirect to login if user is not logged in or guest
    if (!this.loggedInUser || this.loggedInUser?.accountType === 0) {
      this.router.navigate(['login']);
    }
  }

  // add new country
  onCountryAdd(countryForm: NgForm): void {
    const { countryName } = countryForm.value;
    // const { countryMultiplier } = countryForm.value;
    const countryMultiplier = Number(countryForm.value.countryMultiplier);

    // get logged in user's id
    const userId = this.loggedInUser?.id;
    const googleId = this.googleUser?.idToken;

    // create new country object
    const newCountry: Country = { countryName, countryMultiplier };

    if (userId && googleId) {
      this.countryService.addCountry(newCountry, userId, googleId, () => {
        alert(`${newCountry.countryName} with country multiplier ${newCountry.countryMultiplier} was added`)
        countryForm.reset();
        // get countries
        this.countryService.getCountries(() => {
          this.countries = this.countriesFromService;
        });
      });
    }
  }

  // when country is changed in country select
  onCountryChange(event: Country) {
    this.selectedCountry = event;
  }

  // when country multiplier is updated
  onMultiplierUpdate(multiplierForm: NgForm): void {
    const countryName = this.selectedCountry.countryName;
    const id = this.selectedCountry?.id;
    // const { countryMultiplier } = multiplierForm.value;
    const countryMultiplier = Number(
      multiplierForm.value.countryMultiplierEdit
    );

    // get logged in user's id
    const userId = this.loggedInUser?.id;
    const googleId = this.googleUser?.idToken;

    // create new country object
    const updatedCountry: Country = { id, countryName, countryMultiplier };

    if (userId && googleId) {
      this.countryService.updateCountry(
        updatedCountry,
        userId,
        googleId,
        () => {
          alert(`${updatedCountry.countryName} was updated. Country multiplier is now ${updatedCountry.countryMultiplier}`);
          multiplierForm.reset();
          // update countries
          this.countryService.getCountries(() => {
            this.countries = this.countriesFromService;
          });
        }
      );
    }
  }

  // when user type is changed in select
  onUserTypeChange(event: any) {
    this.selectedUserType = event.value;
  }

  // when user is changed in user select for changing account types
  onUserChange(event: User) {
    this.selectedUser = event;
    this.userName =
      this.selectedUser.firstName + ' ' + this.selectedUser.lastName;
  }

  // when user is changed in user select for deleting a user
  onUserChangeDelete(event: User) {
    this.selectedUserDelete = event;
    this.userNameDelete =
      this.selectedUserDelete.firstName + ' ' + this.selectedUserDelete.lastName;
  }

  // change the account type of a user
  onChangeUserSubmit(accountTypeForm: NgForm): void {
    // get selected accountType
    const accountType = Number(this.selectedUserType);
    // get id of user to change
    const selectedUserId = this.selectedUser?.id;
    // get logged in google user's idToken
    const googleUserId = this.googleUser?.idToken;
    // get logged in user's id
    const userId = this.loggedInUser?.id;

    // check if admin is trying to change own account
    if (selectedUserId === userId) {
      alert("You cannot change the account type of your own account")
    } else {
      if (selectedUserId && userId && googleUserId) {
        if (accountType === 1) {
          this.userService.removeUserAdmin(
            selectedUserId,
            userId,
            googleUserId,
            () => {
              alert("Account type was changed to regular user");
              accountTypeForm.reset();
              // update users
              this.userService.getUsers(userId, googleUserId, () => {
                this.users = this.usersFromService;
              });
            }
          );
        } else if (accountType === 2) {
          this.userService.makeUserAdmin(
            selectedUserId,
            userId,
            googleUserId,
            () => {
              alert("Account type was changed to admin");
              accountTypeForm.reset();
              // update users
              this.userService.getUsers(userId, googleUserId, () => {
                this.users = this.usersFromService;
              });
            }
          );
        }
      }
    }
  }

  // delete a user account
  onDeleteUserSubmit(deleteForm: NgForm): void {
    // get id of user to delete
    const deleteUserId = this.selectedUserDelete?.id;
    // get logged in google user's idToken
    const googleUserId = this.googleUser?.idToken;
    // get logged in user's id
    const userId = this.loggedInUser?.id;

    // check if admin is trying to delete own account
    if (deleteUserId === userId) {
      alert("You cannot delete your own account")
    } else {
      const warning_text = `Are you sure you want to delete ${this.userNameDelete} with account Id ${deleteUserId}? This action cannot be undone`;

      if (confirm(warning_text) && deleteUserId && googleUserId && userId) {
        this.userService.deleteUser(deleteUserId, userId, googleUserId, () => {
          alert(`${this.userNameDelete} with account Id ${deleteUserId} was deleted`);
          this.selectedUserDelete = undefined;
          deleteForm.reset();
          // update users
          this.userService.getUsers(userId, googleUserId, () => {
            this.users = this.usersFromService;
          });
        });
      }
    }
  }
}
