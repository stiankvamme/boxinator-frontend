import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { GoogleUser } from 'src/app/models/googleUser.model';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private readonly authenticationService: AuthenticationService,
    private readonly sessionService: SessionService,
    private authService: SocialAuthService
  ) {}

  private showError: boolean | undefined;
  private loggedInUser: User | undefined;
  private twoFA: Boolean | undefined;

  ngOnInit(): void {
    // get logged in user from local storage
    this.loggedInUser = this.sessionService.User;
    this.twoFA = this.sessionService.TwoFA;
    // check if logged in user is admin or regular user, redirect
    if (this.loggedInUser && this.loggedInUser.accountType === 1 && this.twoFA) {
      this.router.navigate(['main']);
    } else if (this.loggedInUser && this.loggedInUser.accountType === 2 && this.twoFA) {
      this.router.navigate(['admin']);
    }
  }

  // redirect guest users to guest page
  public onGuestRegisterClick() {
    this.router.navigate(['guest']);
  }

  //login with google providers, then get user object and set on localstorage, then redirect  
  public externalLogin = () => {
    this.showError = false;
    this.authenticationService.signInWithGoogle().then(
      (res) => {
        const user: GoogleUser = { ...res };
        localStorage.setItem("googleUser", JSON.stringify(user));
        this.authenticationService.googleLogin(user, async () => {
          await this.sessionService.setUser(this.userFromServer);
          const u = this.userFromServer;
          this.userFromServer
          if(u){
            if(u.accountType===0){
              this.router.navigate(['guest']);
            }else if(u.accountType===2){
            this.router.navigate(['verification']);
            } else{
              this.router.navigate(['verification']);
            }
          }
        });
      },
      (error) => console.log(error)
    );
  };

  // get user from authentication service
  get userFromServer(): User | undefined{
    return this.authenticationService.User();
  }
  public clearStorageBeforeLogin(){
    this.sessionService.logOut();
    this.authenticationService.signOutExternal();
    this.authService.signOut(true)
  }
}

