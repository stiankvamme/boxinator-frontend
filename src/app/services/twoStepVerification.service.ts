import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { GoogleUser } from "../models/googleUser.model";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root',
})
export class TwoStepVerificationService {
  private API_URL = environment.twoFactorUrl;
  private _twoFA: Boolean | undefined;
  private _error: string = '';
  public attempting: boolean = false;
  
  constructor(private readonly http: HttpClient) {}

  /**
   * validate verfication code
   * @param googleUser google user that is logged in
   * @param verificationCode verfication code that is sent in
   * @param userId user id of logged in user
   * @returns true/ false dependign on if code is valid
   */
  private _validateVerificationCode(googleUser: GoogleUser, verificationCode: string, userId: number): Observable<Boolean> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUser.idToken}`
    };
    const body = JSON.stringify({
      "sixDigitCode": Number(verificationCode),
      "userId": userId
    });
    return this.http.post<Boolean>(`${this.API_URL}two_factor_verification`, body, {
      headers: headers
    });
  }

  /**
   * get a new verification code
   * @param googleUser google user that is logged in
   * @param userId user id of logged in user
   * @returns 
   */
  private _getNewVerificationCode(googleUser: GoogleUser, userId: number) {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUser.idToken}`
    };
    return this.http.get(`${this.API_URL}new_two_factor_code/${userId}`, {headers : headers});
  }

  // validate verification code
  public validateVerificationCode(googleUser: GoogleUser, verificationCode: string, userId: number, onSuccess: () => void): void {
    this._validateVerificationCode(googleUser, verificationCode, userId)
      .subscribe(
        (okCode: Boolean) => {
          // Success
          if (okCode || !okCode) {
            this._twoFA = okCode;
            onSuccess();
          }
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  // get new verification code
  public getNewVerificationCode (googleUser: GoogleUser, userId: number,onSuccess: () => void): void {
    this._getNewVerificationCode(googleUser, userId)
      .subscribe(
        () => {
          // Success
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   *
   * @returns twoFA boolean
   */
  public TwoFA(): Boolean | undefined {
    return this._twoFA;
  }

  /**
   *
   * @returns returns error message
   */
  public error(): string {
    return this._error;
  }
}