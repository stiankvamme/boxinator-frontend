import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { GoogleUser } from "../models/googleUser.model";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private API_URL = environment.accountsUrl;
  private _user: User | undefined;
  private _error: string = '';
  public attempting: boolean = false;

  constructor(
    private readonly http: HttpClient, 
    private _externalAuthService: SocialAuthService, 
    private route: Router
  ) {}
    //checks if users signin credential is correct with googles login provider
  public signInWithGoogle = ()=> {
    return this._externalAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  //signs out from google 
  public signOutExternal = (hardLogout?: boolean) => {
    this._externalAuthService.signOut(hardLogout);
  }

  /**
   * 
   * @param googleUser google token from logged in google user, and users email to se if user exists
   * idToken to validate google account backend
   * @returns complete user object if user exists 
   */
  private _googleLogin(googleUser: GoogleUser): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUser.idToken}`,
    };
    const body = JSON.stringify({
      "email": googleUser.email,
    });
    return this.http.post<User>(`${this.API_URL}google_auth`, body, {
      headers: headers,
    });
  }

  /**
   * 
   * @param googleUser to get email and idtoken to validate if user exist and if google user is valid 
   * @param onSuccess function to be run on success
   */ 
  public googleLogin(googleUser: GoogleUser, onSuccess: () => void): void {
    this.attempting = true;
    this._googleLogin(googleUser)
      .pipe(
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        (user: User) => {
          // Success
          if (user) {
            this._user = user;
            onSuccess();
          }else{
            this.route.navigate(['register'])
          }
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   *
   * @returns local user object
   */
  public User(): User | undefined {
    return this._user;
  }

  /**
   *
   * @returns returns error messag
   */
  public error(): string {
    return this._error;
  }
}