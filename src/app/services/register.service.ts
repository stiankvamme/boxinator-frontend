import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  private API_URL = environment.accountsUrl;
  private _user: User | undefined;
  private _error: string = '';
  public attempting: boolean = false;

  constructor(private readonly http: HttpClient) {}

  /**
   * 
   * @param user new user object to register
   * @param googleUserToken token from logged in google user
   * @returns user object after registration
   */
   private _registerUser(user: User, googleUserToken: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = JSON.stringify({
      "id": user.id,
      "firstName": user.firstName,
      "lastName": user.lastName,
      "email": user.email,
      "dateOfBirth": user.dateOfBirth,
      "contactNumber": user.contactNumber,
      "country": user.country,
      "postalCode": user.postalCode,
      "accountType": user.accountType
    });
    return this.http.post<User>(`${this.API_URL}register_user`, body, {
      headers: headers
    });
  }

  // register new user
  public registerUser(user: User, googleUserToken: string, onSuccess: () => void): void {
    this.attempting = true;
    this._registerUser(user, googleUserToken)
      .pipe(
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        (userRes: User) => {
          // Success
          if (userRes) {
            this._user = userRes;
            onSuccess();
          }
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * register guest user
   * @param email email of guest user
   * @returns user object for guest, containing only id and email for shipment purposes
   */
  private _registerGuestUser(email: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json'
    };
    const body = JSON.stringify({
     "email": email
    });
    return this.http.post<User>(`${this.API_URL}guest`, body, {
      headers: headers
    });
  }

  // register guest user
  public registerGuestUser(email: string, onSuccess: () => void): void {
    this.attempting = true;
    this._registerGuestUser(email)
      .pipe(
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        (userRes: User) => {
          // Success
          if (userRes) {
            this._user = userRes;
            onSuccess();
          }
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   *
   * @returns local user object
   */
  public User(): User | undefined {
    return this._user;
  }

  /**
   *
   * @returns returns error message
   */
  public error(): string {
    return this._error;
  }
}
