import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Shipment } from '../models/shipment.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MainService {
  private API_URL = environment.shipmentsUrl;
  private _activeShipments: Shipment[] | undefined;
  private _completedShipments: Shipment[] | undefined;
  private _cancelledShipments: Shipment[] | undefined;
  private _userShipments: Shipment[] | undefined;
  private _completedShipmentsForUser: Shipment[] | undefined;
  private _nonCompletedShipments: Shipment[] | undefined;
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  /**
   * 
   * @param userId userId of the logged in user
   * @param googleUserToken logged in google user id token
   * @returns list of active shipments
   */
  private _getActiveShipments(userId: number, googleUserToken: string): Observable<Shipment[]> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<Shipment[]>(`${this.API_URL}active/${userId}`, {
      headers: headers
    });
  }

  // get all active shipments
  public getActiveShipments (userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._getActiveShipments(userId, googleUserToken)
      .subscribe(
        (shipments: Shipment[]) => {
          // Success
            this._activeShipments = shipments;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * get all completed shipments
   * @param userId userId of the logged in user
   * @param googleUserToken logged in google user id token
   * @returns list of completed shipments
   */
  private _getCompletedShipments(userId: number, googleUserToken: string): Observable<Shipment[]> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<Shipment[]>(`${this.API_URL}completed/${userId}`, {
      headers: headers
    });
  }

  // get all completed shipments
  public getCompletedShipments(userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._getCompletedShipments(userId, googleUserToken)
      .subscribe(
        (shipments: Shipment[]) => {
          // Success
            this._completedShipments = shipments;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
 * get all cancelled shipments
 * @param userId userId of the logged in user
 * @param googleUserToken logged in google user id token
 * @returns list of cancelled shipments
 */
  private _getCancelledShipments(userId: number, googleUserToken: string): Observable<Shipment[]> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<Shipment[]>(`${this.API_URL}cancelled/${userId}`, {
      headers: headers
    });
  }
  
  // get all cancelled shipments
  public getCancelledShipments(userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._getCancelledShipments(userId, googleUserToken)
      .subscribe(
        (shipments: Shipment[]) => {
          // Success
            this._cancelledShipments = shipments;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * get shipments by user ID from api
   * @param id userId of user you want to get shipments for
   * @param userId userId of the logged in user
   * @param googleUserToken logged in google user id token
   * @returns specific users shipments
   */
  private _getShipmentsForUser(id: number, userId: number, googleUserToken: string): Observable<Shipment[]> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<Shipment[]>(`${this.API_URL}customer/${id}/${userId}`, {
      headers: headers
    }); 
  }

  // get shipments by user ID
  public getShipmentsForUser(id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._getShipmentsForUser(id, userId, googleUserToken)
      .subscribe(
        (shipments: Shipment[]) => {
          // Success
            this._userShipments = shipments;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * get completed shipments for a specific user by id
   * @param id userId of the user you want to get shipments for
   * @param userId userId for the logged in user
   * @param googleUserToken logged in google user id token
   * @returns all completed shipments
   */
  private _getCompletedShipmentsForUser(id: number, userId: number, googleUserToken: string): Observable<Shipment[]> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<Shipment[]>(`${this.API_URL}completed/${id}/${userId}`, 
    {
      headers: headers
    });
  }

  // get completed shipments for a specific user by id
  public getCompletedShipmentsForUser(id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._getCompletedShipmentsForUser(id, userId, googleUserToken)
      .subscribe(
        (shipments: Shipment[]) => {
          // Success
            this._completedShipmentsForUser = shipments;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

    /**
   * get all non-completed shipments for a specific user by id
   * @param id userId of the user you want to get shipments for
   * @param userId userId for the logged in user
   * @param googleUserToken logged in google user id token
   * @returns all non completed shipments
   */
     private _getNonCompletedShipments(id: number, userId: number, googleUserToken: string): Observable<Shipment[]> {
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${googleUserToken}`
      };
      return this.http.get<Shipment[]>(`${this.API_URL}non-completed/${id}/${userId}`, 
      {
        headers: headers
      });
    }
  
    // get all non-completed shipments for a specific user by id
    public getNonCompletedShipments (id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
      this._getNonCompletedShipments(id, userId, googleUserToken)
        .subscribe(
          (shipments: Shipment[]) => {
            // Success
              this._nonCompletedShipments = shipments;
              onSuccess();
          },
          (error: HttpErrorResponse) => {
            // error
            this._error = error.message;
          }
        );
    }

  /**
   * update shipment - for regular users
   * @param shipment updated shipment object 
   * @param userId id of logged in user
   * @param googleUserToken logged in google user id token
   * @returns 
   */
  private _updateShipment(shipment: Shipment, userId: number, googleUserToken: string): Observable<Shipment> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = JSON.stringify({
      "id": shipment.id,
      "receiverName": shipment.receiverName,
      "status": shipment.status,
      "destinationCountry": shipment.destinationCountry,
      "weight_kg": shipment.weight_kg,
      "boxColour": shipment.boxColour,
      "price": shipment.price,
      "accountId": shipment.accountId
    });
    return this.http.put<Shipment>(`${this.API_URL}user_update/cancel/${userId}`, body, {
      headers: headers
    });
  }

  // update shipment - for regular users
  public updateShipment(shipment: Shipment, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._updateShipment(shipment, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * update a shipment in api - for admins
   * @param shipment updated shipment object 
   * @param userId id of logged in user
   * @param googleUserToken logged in google user id token
   * @returns 
   */
  private _updateShipmentAdmin(shipment: Shipment, userId: number, googleUserToken: string): Observable<Shipment> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = JSON.stringify({
      "id": shipment.id,
      "receiverName": shipment.receiverName,
      "status": shipment.status,
      "destinationCountry": shipment.destinationCountry,
      "weight_kg": shipment.weight_kg,
      "boxColour": shipment.boxColour,
      "price": shipment.price,
      "accountId": shipment.accountId
    });
    return this.http.put<Shipment>(`${this.API_URL}admin_update/${shipment.id}/${userId}`, body, {
      headers: headers
    });
  }

  // update shipment - for admins
  public updateShipmentAdmin(shipment: Shipment, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._updateShipmentAdmin(shipment, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * add shipment to api
   * @param shipment updated shipment object
   * @returns 
   */
  private _addShipment(shipment: Shipment): Observable<Shipment> {
    const headers = {
      'Content-Type': 'application/json',
    };
    const body = JSON.stringify({
      "receiverName": shipment.receiverName,
      "status": shipment.status,
      "destinationCountry": shipment.destinationCountry,
      "weight_kg": shipment.weight_kg,
      "boxColour": shipment.boxColour,
      "price": shipment.price,
      "accountId": shipment.accountId
    });
    return this.http.post<Shipment>(`${this.API_URL}create`, body, {
      headers: headers
    });
  }

  // add shipment
  public addShipment(shipment: Shipment, onSuccess: () => void): void {
    this._addShipment(shipment)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   *  delete shipment from api
   * @param shipmentId id of shipment to be deleted
   * @param googleUserToken logged in google user id token
   * @param userId id of logged in user
   * @returns 
   */
  private _deleteShipment(shipmentId: number, userId: number, googleUserToken: string): Observable<Shipment> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.delete<Shipment>(`${this.API_URL}${shipmentId}/${userId}` , {
      headers: headers
    });
  }

  // delete shipment
  public deleteShipment(shipmentId: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._deleteShipment(shipmentId, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }
 
  // return active shipments array
  public activeShipments(): Shipment[] | undefined {
    return this._activeShipments;
  }

  // return completed shipments array
  public completedShipments(): Shipment[] | undefined {
    return this._completedShipments;
  }

  // return completed shipments array
  public cancelledShipments(): Shipment[] | undefined {
    return this._cancelledShipments;
  }

  // return a users shipments array
  public userShipments(): Shipment[] | undefined {
    return this._userShipments;
  }

  // return completed shipments array
  public completedShipmentsForUser(): Shipment[] | undefined {
    return this._completedShipmentsForUser;
  }

  // return non-completed shipments array
  public nonCompletedShipments(): Shipment[] | undefined {
    return this._nonCompletedShipments;
  }

  // return error
  public error(): string {
    return this._error;
  }
}
