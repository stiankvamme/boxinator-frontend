import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root',
  })
export class RgbaValidationService {

    constructor(){}
    
    /**
     * Validate RGBA 
     * @param rgbaIn from input
     * calls on validated numbers
     * @returns rgba string if RGBA valid else empty string ""
     */
    public validateRGBA(rgbaIn: string): string {
        const rgba = rgbaIn.slice(0, 4);
        const rgb = rgbaIn.slice(0, 3);
        let values: string[]=[];

        if (rgba === 'rgba') {
            values = rgbaIn.substring(5, rgbaIn.length-1).split(",");
        } else if (rgb == 'rgb') {
            values = rgbaIn.substring(4, rgbaIn.length-1).split(",");
        }

        const validatedRGBA = this.validateNumbers(values);
        return validatedRGBA;
    }

    // validate numbers in rgba
    private validateNumbers(arr: any[]): string {
        let str = "";
        let isRgb=true;

        if (arr) {
            if(arr.length===4){
                isRgb=false;
            }
            str = 'rgba(';
            for (let i = 0; i < arr.length; i++) {
                const num = Number(arr[i]);
                if (!isNaN(num) && i < 3 && num >= 0 && num < 256) {
                    str = str + num + ',';
                }
                if (i === 3) {
                    str = str + num + ')';
                }
            }
            if (isRgb) {
                str = str + 1 + ')';
            }
        }
        return str;
    }
}
