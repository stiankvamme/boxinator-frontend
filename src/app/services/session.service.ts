import { Injectable } from '@angular/core';
import { GoogleUser } from '../models/googleUser.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private _googleUser: GoogleUser | undefined;
  private _user: User | undefined;
  private _TwoFA: Boolean | undefined;

  //gets from localstorage if there is an object and sets on sessionservice's state
  constructor() {
    const storedGoogleUser = localStorage.getItem('googleUser');
    const storedUser = localStorage.getItem('user');
    const storedTwoFA = localStorage.getItem('twoFA');
    //checks if there is a stored google user, if then gets it and sets it on state
    if (storedGoogleUser) {
      this._googleUser = JSON.parse(storedGoogleUser) as GoogleUser;
    }
    //checks if there is a stored user, if then gets it and sets it on state
    if (storedUser) {
      this._user = JSON.parse(storedUser) as User;
    }
    //checks if there is a stored twoFA, if then gets it and sets it on state
    if (storedTwoFA) {
      this._TwoFA = JSON.parse(storedTwoFA) as Boolean;
    }
  }

  //get googleUser from sessionservice's state
  get googleUser(): GoogleUser | undefined {
    return this._googleUser;
  }

  //set new google user on sessionstorage and state
  setGoogleUser(googleUser: any | undefined): void {
    this._googleUser = googleUser;
    localStorage.setItem('googleUser', JSON.stringify(googleUser));
  }

  //get user from session storages state
  get User(): User | undefined {
    return this._user;
  }

  //set new user on sessionstorage and state
  setUser(user: any | undefined): void {
    this._user = user;
    localStorage.setItem('user', JSON.stringify(user));
  }
 
  //get twoFA true/false from sessionservice state
  get TwoFA(): Boolean | undefined {
    return this._TwoFA;
  }

  //set new twoFa true/false on localstorage and sessionstorage state
  setTwoFA(twofa: any | undefined): void {
    localStorage.setItem('twoFA', JSON.stringify(twofa));
  }

  //set user to undefine to update components state
  //then clear localstorage
  logOut() {
    this.setUser(undefined);
    localStorage.clear();
  }
}
