import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})

export class UserService {
  private API_URL = environment.accountsUrl;
  private _user: User | undefined;
  private _users: User[] | undefined;
  private _error: string = '';
  public attempting: boolean = false;

  constructor(private readonly http: HttpClient) {}

  /**
   * 
   * @param id userId of user to get from api
   * @param userId userId of logged in user
   * @param googleUserToken google user id token of logged in user
   * @returns 
   */
  private _getUser(id: number, userId: number, googleUserToken: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<User>(`${this.API_URL}${id}/${userId}` , {
      headers: headers
    });
  }

  // get user by userId
  public getUser(id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this.attempting = true;

    this._getUser(id, userId, googleUserToken)
      .pipe(
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        (user: User) => {
          // Success
          if (user.id) {
            this._user = user;
            onSuccess();
          }
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * @param userId userId of logged in user
   * @param googleUserToken google user id token of logged in user
   * @returns 
   */
  private _getUsers(userId: number, googleUserToken: string) {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.get<User[]>(`${this.API_URL}all_accounts/${userId}`, {
      headers: headers
    });
  }

  // get all users
  public getUsers (userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._getUsers(userId, googleUserToken)
      .subscribe(
        (users: User[]) => {
          // Success
            this._users = users;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * 
   * @param user updated user object
   * @param googleUserToken google user id token of logged in user
   * @returns 
   */
  private _updateUser(user: User, googleUserToken: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = JSON.stringify({
      "id": user.id,
      "firstName": user.firstName,
      "lastName": user.lastName,
      "email": user.email,
      "dateOfBirth": user.dateOfBirth,
      "contactNumber": user.contactNumber,
      "country": user.country,
      "postalCode": user.postalCode
    });
    return this.http.put<User>(`${this.API_URL}${user.id}`, body, {
      headers: headers
    });
  }

  // update user
  public updateUser(user: User, googleUserToken: string, onSuccess: () => void): void {
    this.attempting = true;
    this._updateUser(user, googleUserToken)
      .pipe(
        finalize(() => {
          this.attempting = false;
        })
      )
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * Change account type from admin to regular user
   * @param id UserId of user to remove admin rights from
   * @param userId UserId of logged in user
   * @param googleUserToken google user id token of logged in user
   */
  private _removeUserAdmin(id: number, userId: number, googleUserToken: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = {};
    return this.http.put<User>(`${this.API_URL}remove_admin/${id}/${userId}`, body, {
      headers: headers
    });
  }

  // change account type from admin to regular user
  public removeUserAdmin(id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._removeUserAdmin(id, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * Change account type regular user to admin
   * @param id UserId of user to remove admin rights from
   * @param userId UserId of logged in user
   * @param googleUserToken google user id token of logged in user
   */
  private _makeUserAdmin(id: number, userId: number, googleUserToken: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = {};
    return this.http.put<User>(`${this.API_URL}make_admin/${id}/${userId}`, body, {
      headers: headers
    });
  }
  
  // change account type from regular user to admin user
  public makeUserAdmin(id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._makeUserAdmin(id, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * 
   * @param id userId of the account to be deleted
   * @param userId userId of the account logged in
   * @param googleUserToken google user id token of logged in user
   * @returns 
   */
  private _deleteUser(id: number, userId: number, googleUserToken: string): Observable<User> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    return this.http.delete<User>(`${this.API_URL}${id}/${userId}`, {
      headers: headers
    });
  }

  // delete user
  public deleteUser(id: number, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._deleteUser(id, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  // returns user object
  public user(): User | undefined {
    return this._user;
  }

  // returns array of users
  public users(): User[] | undefined {
    return this._users;
  }

  // returns error message
  public error(): string {
    return this._error;
  }
}
