import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Country } from '../models/country.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CountryService {
  private API_URL = environment.countriesUrl;
  private _countries: Country[] | undefined;
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  // get all countries from api
  private _getCountries() {
    return this.http.get<Country[]>(this.API_URL);
  }

  // get countries
  public getCountries (onSuccess: () => void): void {
    this._getCountries()
      .subscribe(
        (countries: Country[]) => {
          // Success
            this._countries = countries;
            onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * update country by id
   * @param country updated country object to update api
   * @param userId userId of logged in user
   * @param googleUserToken logged in google user id token
   * @returns contry object
   */
  private _updateCountry(country: Country, userId: number, googleUserToken: string): Observable<Country> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = JSON.stringify({
      "id": country.id,
      "countryName": country.countryName,
      "countryMultiplier": country.countryMultiplier
    });
    return this.http.put<Country>(`${this.API_URL}update/${country.id}/${userId}`, body, {
      headers: headers
    });
  }

  // update country
  public updateCountry(country: Country, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._updateCountry(country, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  /**
   * add new country to api
   * @param country country object to add to api
   * @param userId userId of logged in user
   * @param googleUserToken logged in google user id token
   * @returns contry object
   */
  private _addCountry(country: Country, userId: number, googleUserToken: string): Observable<Country> {
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${googleUserToken}`
    };
    const body = JSON.stringify({
      "countryName": country.countryName,
      "countryMultiplier": country.countryMultiplier
    });
    return this.http.post<Country>(`${this.API_URL}add/${userId}`, body, {
      headers: headers
    });
  }

  // add country
  public addCountry(country: Country, userId: number, googleUserToken: string, onSuccess: () => void): void {
    this._addCountry(country, userId, googleUserToken)
      .subscribe(
        () => {
          // Success
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          // error
          this._error = error.message;
        }
      );
  }

  // return country array
  public countries(): Country[] | undefined {
    return this._countries;
  }

  // return error message
  public error(): string {
    return this._error;
  }
}